// $id Javascript portion for the XMPP Client
var ROSTERWINOPTS = 'top=25, width=375, height=450, location=0, scrollbars=1, resizable=1, status=0';
var xmppclient_presence = new Array('available', 'unavailable', 'chat', 'busy', 'dnd', 'away', 'xa', 'online', 'offline');
var timeout = '';
var curl = '';
var xcpass = '';

$(document).ready(function() {
  $('ul li a').each(function() {
    if ($(this).html() == 'Start Client') {
      var url = $(this).attr('href');
      $(this).attr('href', 'javascript:xmppclient_menu_login("' + url + '")');
      return false;
    };
  });
});

/**
 * Add mouseover capability to the online user DOM elements
 * @param {String} url The url to call in order to display user information
 */
function xmppclient_onlineuser_mouseover(url) {
  // add the div we need to the body so as to avoid the offset issues in Javascript
  if ($('#xmppclient_onlineuserinfo').size() == 0) {
    $(document).find('body').append('<div class="xmppclient_onlineuserinfo"></div>');
  }
  // add mouseover and mouseout to the div that pops up with the pertinent information
  $('.xmppclient_onlineuserinfo').mouseout(function() {
    timeout = window.setTimeout(function() {
                                  $('.xmppclient_onlineuserinfo')
                                  .html('')
                                  .css({ top: '1000px', left: '1000px' })
                                  .hide();
                                }, 1000);
  }).mouseover(function() {
    window.clearTimeout(timeout);
  });
  // add click functionality to the appropriate information icon
  $('.xmppclient_onlineuser_icon').unbind().click(function(e) {
    var id = $(this).parent().parent().attr('id'); // tr will have the id that is required to make the ajax request
    $('.xmppclient_onlineuserinfo').css({ top: e.pageY + 'px', left: (e.pageX + 15) + 'px' });
    $.get(url + '/' + id, function(result) {
      $('.xmppclient_onlineuserinfo').html(result).show('slow');
      $('.xmppclient_onlineuserinfo_tablediv').mouseover(function() {
        $(this).addClass('xmppclient_onlineuserinfo_tabledivover');
      }).mouseout(function() {
        $(this).removeClass('xmppclient_onlineuserinfo_tabledivover');
      });
    });
  });
}

/**
 * Log person into the Thin XMPP Client on the website
 * @param url {String}
 *      The url to call in order to activate the client
 * @paeam username {String}
 *      The username to log into the client with
 * @param resource {String}
 *      The resource i.e. webclient, laptop etc that you login with
 * @param domain {String}
 *      The domain you are logging into with the client
 * @param srvUrl {String}
 *      This is the server url so the client knows where to send requests too
 * @param httpbase {String}
 *      This is the httpbase the client should use, i.e. polling or binding
 * @param authtype {String}
 *      The authentication type the client should use i.e. sasl, nonsasl
 * @param locale {String}
 *      The language the client should start with
 * @param muc_conference {String}
 *      The conference server for the multi user chat rooms
 * @param search {String}
 *      The server that jabber user search occurs at
 */
function xmppclient_login(url, username, resource, domain, srvUrl, httpbase, authtype, locale, muc_conference, search) {
  url += '&username=' + username + '&resource=' + resource + '&domain=' + domain + '&srvUrl=' + escape(srvUrl) + '&httpbase=' + httpbase;
  url += '&authtype=' + authtype + '&locale=' + locale + '&MUC=' + muc_conference + '&SEARCH=' + search;
  $.get(srvUrl + '/password', function(result) {
    xcpass = result;
    var w = window.open(url, 'XMPPCLIENTROSTERWIN', ROSTERWINOPTS);
    w.focus();
    result = '';
  });
  return false;
}

/**
 * @param el {String}
 *      The DOM element we will manipulate if there
 * @param status {String}
 *      The status of the user
 */
function xmppclient_presence_update(el, status) {
  if ($(el).size() > 0) {
    if (status == 'offline' || status == 'unavailable') {
      $(el).each(function() {
        $(this).css({display: 'none'});
        var e = $(this).parent().children('div.xmppclient');
        $.each(xmppclient_presence, function(k, v) {
          e.removeClass(v);
        });
        e.addClass(status);
      });
    } else {
      $(el).each(function() {
        $(this).css({display: 'inline'});
        var e = $(this).parent().children('div.xmppclient');
        $.each(xmppclient_presence, function(k, v) {
          e.removeClass(v);
        });
        e.addClass(status);
      });
    };
  };
};

/**
 * @param url {String}
 *      Url where to get the necessary presence updates for the user
 */
function xmppclient_presence_update_get(url, timeout) {
  $.get(url, function(result) { eval(result); });
  setTimeout(function() { xmppclient_presence_update_get(url, timeout); }, timeout);
}

/**
 * Call the xmppclient_login function from the menu once we have the relevant information from the post
 * @param url {String}
 *      The url to call in order to start the client
 */
function xmppclient_menu_login(url) {
  $.get(url, function(result) {
    eval(result);
  });
};

/**
 * Log person into the thin client and then open a one on one chat with the jid that is their buddy
 * @param url {String}
 *      The url to call in order to start the client.
 * @param username {String}
 *      The user name to log into the client with.
 * @param resource {String}
 *      The resource with which to log into the client with.
 * @param domain {String}
 *      The domain you are logging into.
 * @param srvUrl {String}
 *      The server url so the client can send requests to it if you wish to configure the client for that.
 * @param buddy {String}
 *      The jid of the person you wish to initiate the one on one chat with.
 * @param httpbase {String}
 *      This is the httpbase the client should use, i.e. polling or binding
 * @param authtype {String}
 *      The authentication type the client should use i.e. sasl, nonsasl
 * @param locale {String}
 *      The language the client should start with
 * @param muc_conference {String}
 *      The conference server for the multi user chat rooms
 * @param search {String}
 *      The server that jabber user search occurs at
 */
function xmppclient_message_chat(url, username, resource, domain, srvUrl, buddy, httpbase, authtype, locale, muc_conference, search) {
  w = window.open('', 'XMPPCLIENTROSTERWIN', ROSTERWINOPTS);
  w.blur();
  window.focus();
  if (w.con && w.con.connected()) {
    w.focus();
    w.xcMsgUser(buddy);
  } else {
    w.close();
    url += '&username=' + username + '&resource=' + resource + '&domain=' + domain + '&buddy=' + escape(buddy) + '&srvUrl=' + escape(srvUrl);
    url += '&httpbase=' + httpbase + '&authtype=' + authtype + '&locale=' + locale + '&MUC=' + muc_conference + '&SEARCH=' + search;
    $.get(srvUrl + '/password', function(result) {
      xcpass = result;
      var w = window.open(url, 'XMPPCLIENTROSTERWIN', ROSTERWINOPTS);
      w.focus();
      result = '';
    });
  }
}
