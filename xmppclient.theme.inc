<?php
// $id

/**
 * Builds display block for XMPP Client Login
 */
function theme_xmppclient_login() {
  global $user;
  $result = db_fetch_object(db_query("SELECT * FROM {xmppclient_user} WHERE uid = %d", $user->uid));
  // check if the password is already stored or not in the system, if not direct user to the enter portion
  if (!$result->password) {
    $output = '<div id="xmppclient-login" class="xmppclient_login">'. l(t('Configure Client'), 'user/'. $user->uid .'/edit') .'</div>';
  }
  else {
    $jid = split("@", $result->jid);
    $username = $jid[0];
    $domain = $jid[1];
    $url = base_path() . XMPPCLIENT_PATH .'/xwchat/roster.html?';
    // if no resource we give it the username resource since the user might have been created before the client
    $resource = (isset($user->xmppclient['resource']) && $user->xmppclient['resource'] != '') ? $user->xmppclient['resource'] : $user->name;
    $config_url = url('xmppclient');
    $locale = __xmppclient_determine_locale($user); // retrieving the locale to use for the client
    $output = '<div id="xmppclient-login" class="xmppclient_login">';
    $output .= drupal_get_form('xmppclient_login_form', $username, $domain, $url, $resource, $config_url, $locale);
    $output .= '</div>';
  }
  return $output;
}
